package com.example.EObrazovanje2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EObrazovanje2022Application {

	public static void main(String[] args) {
		SpringApplication.run(EObrazovanje2022Application.class, args);
	}

}
