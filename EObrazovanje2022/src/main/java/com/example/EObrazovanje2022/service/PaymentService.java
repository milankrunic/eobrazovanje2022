package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.Payment;
import com.example.EObrazovanje2022.repository.PaymentRepository;
import com.example.EObrazovanje2022.serviceInterface.PaymentServiceInterface;

@Service
public class PaymentService implements PaymentServiceInterface {

	@Autowired
	PaymentRepository repository;
	
	@Override
	public List<Payment> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Payment findById(Long id) {
		// TODO Auto-generated method stub
//		return repository.getOne(id);
		return null;
	}

	@Override
	public Payment save(Payment payment) {
		// TODO Auto-generated method stub
		return repository.save(payment);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Page<Payment> findByUsername(String username,Pageable page) {
		// TODO Auto-generated method stub
		return repository.findByAccount_student_user_username(username,page);
	}

	@Override
	public Long countByUsername(String username) {
		// TODO Auto-generated method stub
		return repository.countByAccount_student_user_username(username);
	}

}
