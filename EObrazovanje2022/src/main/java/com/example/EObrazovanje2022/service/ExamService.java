package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.Exam;
import com.example.EObrazovanje2022.repository.ExamRepository;
import com.example.EObrazovanje2022.serviceInterface.ExamServiceInterface;

@Service
public class ExamService implements ExamServiceInterface {

	@Autowired
	ExamRepository examRepository;
	
	@Override
	public List<Exam> findAll() {
		return examRepository.findAll();
	}

	@Override
	public Exam findById(Long id) {
//		return examRepository.getOne(id);
		return null;
	}

	@Override
	public Exam save(Exam exam) {
		return examRepository.save(exam);
	}

	@Override
	public void delete(Long id) {
		examRepository.deleteById(id);
	}

	@Override
	public List<Exam> examPassedForStudent(String cardNum) {
		return examRepository.findByEnrollment_student_cardNumber(cardNum);
	}

	@Override
	public Exam examOneByUsernameAndId(String username, Long id) {
		return examRepository.findOneByEnrollment_student_user_usernameAndId(username, id);
	}

	@Override
	public List<Exam> findByCourseInstance(Long courseId) {
		return examRepository.findByEnrollment_courseInstance_id(courseId);
	}

	@Override
	public Long countForStudent(String username) {
		return examRepository.countByEnrollment_student_user_username(username);
	}

	@Override
	public Page<Exam> examForStudent(String cardNum, Pageable page) {
		return examRepository.findByEnrollment_student_cardNumber(cardNum, page);
	}

}

