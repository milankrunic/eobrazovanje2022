package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.ExamPartType;
import com.example.EObrazovanje2022.repository.ExamPartTypeRepository;
import com.example.EObrazovanje2022.serviceInterface.ExamPartTypeServiceInterface;

@Service
public class ExamPartTypeService implements ExamPartTypeServiceInterface {

	@Autowired
	ExamPartTypeRepository repository;
	
	@Override
	public List<ExamPartType> findAll() {
		return repository.findAll();
	}

	@Override
	public ExamPartType findById(Long id) {
//		return repository.getOne(id);
		return null;
	}

	@Override
	public ExamPartType save(ExamPartType examPart) {
		return repository.save(examPart);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

	@Override
	public ExamPartType findByCode(String code) {
		return repository.findOneByCode(code);
	}

}
