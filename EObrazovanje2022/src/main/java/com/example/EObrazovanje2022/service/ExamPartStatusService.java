package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.ExamPartStatus;
import com.example.EObrazovanje2022.repository.ExamPartStatusRepository;
import com.example.EObrazovanje2022.serviceInterface.ExamPartStatusServiceInterface;

@Service
public class ExamPartStatusService implements ExamPartStatusServiceInterface {

	@Autowired
	ExamPartStatusRepository repository;
	
	@Override
	public List<ExamPartStatus> findAll() {
		return repository.findAll();
	}

	@Override
	public ExamPartStatus findById(Long id) {
//		return repository.getOne(id);
		return null;
	}

	@Override
	public ExamPartStatus save(ExamPartStatus examPart) {
		return repository.save(examPart);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

	@Override
	public ExamPartStatus expsByCode(String code) {
		return repository.findOneByCode(code);
	}

}

