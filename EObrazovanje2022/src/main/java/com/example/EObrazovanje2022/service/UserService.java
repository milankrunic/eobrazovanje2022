package com.example.EObrazovanje2022.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.User;
import com.example.EObrazovanje2022.repository.UserRepository;
import com.example.EObrazovanje2022.serviceInterface.UserServiceI;

@Service
public class UserService implements UserServiceI {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public Page<User> findAll(Pageable page) {
		return userRepository.findAll(page);
	}

	@Override
	public User findById(Long id) {
//		return userRepository.getOne(id);
		return null;
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public void delete(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findOneByUsername(username);
	}

	@Override
	public Long count() {
		return userRepository.count();
	}
}
