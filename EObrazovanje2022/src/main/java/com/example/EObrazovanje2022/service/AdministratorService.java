package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.Administrator;
import com.example.EObrazovanje2022.repository.AdministratorRepository;
import com.example.EObrazovanje2022.serviceInterface.AdministratorServiceI;

@Service
public class AdministratorService implements AdministratorServiceI {

	@Autowired
	AdministratorRepository administratorRepository;
	
	@Override
	public List<Administrator> findAll() {
		return administratorRepository.findAll();
	}

	@Override
	public Administrator findById(Long id) {
//		return administratorRepository.getOne(id);
		return null;
	}

	@Override
	public Administrator save(Administrator administrator) {
		return administratorRepository.save(administrator);
	}

	@Override
	public void delete(Long id) {
		administratorRepository.deleteById(id);
	}

	@Override
	public Administrator findByUser(String username) {
		return administratorRepository.findOneByUser_username(username);
	}

}
