package com.example.EObrazovanje2022.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.CourseSpecification;
import com.example.EObrazovanje2022.repository.CourseSpecificationRepository;
import com.example.EObrazovanje2022.serviceInterface.CourseSpecificationServiceInterface;

@Service
public class CourseSpecificationService implements CourseSpecificationServiceInterface {

	@Autowired
	CourseSpecificationRepository csrepos;
	
	@Override
	public Page<CourseSpecification> findAll(Pageable page) {
		// TODO Auto-generated method stub
		return csrepos.findAll(page);
	}

	@Override
	public CourseSpecification findById(Long id) {
		// TODO Auto-generated method stub
//		return csrepos.getOne(id);
		return null;
	}

	@Override
	public CourseSpecification save(CourseSpecification courseSpecification) {
		// TODO Auto-generated method stub
		return csrepos.save(courseSpecification);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		csrepos.deleteById(id);
	}

	@Override
	public Long count() {
		// TODO Auto-generated method stub
		return csrepos.count();
	}

	@Override
	public Page<CourseSpecification> findAll(String searchString, Pageable page) {
		// TODO Auto-generated method stub
		return csrepos.findAll(searchString, page);
	}

}

