package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.TypeDocument;
import com.example.EObrazovanje2022.repository.TypeDocumentRepository;
import com.example.EObrazovanje2022.serviceInterface.TypeDocumentServiceInterface;

@Service
public class TypeDocumentService implements TypeDocumentServiceInterface {

	@Autowired
	TypeDocumentRepository typeDocumentRepository;
	
	@Override
	public List<TypeDocument> findAll() {
		// TODO Auto-generated method stub
		return typeDocumentRepository.findAll();
	}

	@Override
	public TypeDocument findById(Long id) {
		// TODO Auto-generated method stub
//		return typeDocumentRepository.getOne(id);
		return null;
	}

	@Override
	public TypeDocument save(TypeDocument typeDocument) {
		// TODO Auto-generated method stub
		return typeDocumentRepository.save(typeDocument);
	}

	@Override
	public void delete(Long id) {
		typeDocumentRepository.deleteById(id);
	}

	@Override
	public TypeDocument typeDocByCode(String code) {
		// TODO Auto-generated method stub
		return typeDocumentRepository.findOneByCode(code);
	}

}
