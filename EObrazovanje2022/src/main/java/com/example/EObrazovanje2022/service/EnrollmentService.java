package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.Enrollment;
import com.example.EObrazovanje2022.repository.EnrollmentRepository;
import com.example.EObrazovanje2022.serviceInterface.EnrollmentServiceInterface;

@Service
public class EnrollmentService implements EnrollmentServiceInterface {

	@Autowired
	EnrollmentRepository er;
	
	@Override
	public List<Enrollment> findAll() {
		// TODO Auto-generated method stub
		return er.findAll();
	}

	@Override
	public Enrollment findById(Long id) {
		// TODO Auto-generated method stub
//		return er.getOne(id);
		return null;
	}

	@Override
	public Enrollment save(Enrollment e) {
		// TODO Auto-generated method stub
		return er.save(e);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		er.deleteById(id);
	}

	@Override
	public Enrollment findByCourseInstanceAndStudent(Long idCourseInstance, String cardNumber) {
		return er.findByCourseInstance_idAndStudent_cardNumber(idCourseInstance, cardNumber);
	}

}

