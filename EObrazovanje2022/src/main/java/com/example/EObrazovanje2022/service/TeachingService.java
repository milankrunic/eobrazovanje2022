package com.example.EObrazovanje2022.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.EObrazovanje2022.model.Teaching;
import com.example.EObrazovanje2022.repository.TeachingRepository;
import com.example.EObrazovanje2022.serviceInterface.TeachingServiceI;

@Service
public class TeachingService implements TeachingServiceI {

	@Autowired
	TeachingRepository tr;
	
	@Override
	public List<Teaching> findAll() {
		// TODO Auto-generated method stub
		return tr.findAll();
	}

	@Override
	public Teaching getOne(Long id) {
		// TODO Auto-generated method stub
//		return tr.getOne(id);
		return null;
	}

	@Override
	public Teaching save(Teaching te) {
		// TODO Auto-generated method stub
		return tr.save(te);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		tr.deleteById(id);
	}

	@Override
	public List<Teaching> findByUsername(String username) {
		// TODO Auto-generated method stub
		return tr.findByTeacher_user_username(username);
	}

	@Override
	public Teaching findByCourseInstance(Long courseId) {
		// TODO Auto-generated method stub
		return tr.findByCourseInstance_id(courseId);
	}

}
