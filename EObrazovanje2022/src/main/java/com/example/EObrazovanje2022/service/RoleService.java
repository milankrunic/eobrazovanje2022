package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.EObrazovanje2022.model.Role;
import com.example.EObrazovanje2022.repository.RoleRepository;
import com.example.EObrazovanje2022.serviceInterface.RoleServiceI;

@Service
public class RoleService implements RoleServiceI {

	@Autowired
	RoleRepository repository;
	
	@Override
	public List<Role> findAll() {
		return repository.findAll();
	}

	@Override
	public Role findById(Long id) {
//		return repository.getOne(id);
		return null;
	}

	@Override
	public Role save(Role userRole) {
		return repository.save(userRole);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Role findByCode(String code) {
		return repository.findOneByCode(code);
	}

}
