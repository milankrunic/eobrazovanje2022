package com.example.EObrazovanje2022.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.EObrazovanje2022.model.ExamPart;
import com.example.EObrazovanje2022.repository.ExamPartRepository;
import com.example.EObrazovanje2022.serviceInterface.ExamPartServiceInterface;

@Service
@Transactional
public class ExamPartService implements ExamPartServiceInterface {
	
	@Autowired
	ExamPartRepository examPartRepository;
	
	@Override
	public List<ExamPart> findAll() {
		return examPartRepository.findAll();
	}
	
	@Override
	public List<ExamPart> findByCode(String code) {
		return examPartRepository.findByCode(code);
	}

	@Override
	public ExamPart findById(Long id) {
//		return examPartRepository.getOne(id);
		return null;
	}

	@Override
	public ExamPart save(ExamPart examPart) {
		return examPartRepository.save(examPart);
	}

	@Override
	public void delete(Long id) {
		examPartRepository.deleteById(id);
	}

	@Override
	public List<ExamPart> examPartPassedForStudent(String cardNumber) {
		return examPartRepository.findOneByExam_enrollment_student_cardNumber(cardNumber);
	}

	@Override
	public List<ExamPart> findByCodeAndCardNum(String code, String cardNum) {
		return examPartRepository.findByExamPartStatus_codeAndExam_enrollment_student_cardNumber(code, cardNum);
	}

	@Override
	public Page<ExamPart> findByCardNumAndCourse(String cardNum, Long id,Pageable page) {
		return examPartRepository.findByExam_enrollment_student_cardNumberAndExam_enrollment_courseInstance_id(cardNum, id,page);
	}

	@Override
	public Page<ExamPart> findByCourseInstance(Long courseId,Pageable page) {
		return examPartRepository.findByCourseInstance(courseId,page);
	}
	
	@Override
	public List<ExamPart> findByCourseInstance(Long courseId) {
		return examPartRepository.findByCourseInstance(courseId);
	}

	@Override
	public long maxId() {
		return examPartRepository.maxId();
	}

	@Override
	public Page<ExamPart> findByTeacher(String username,Pageable page) {
		return examPartRepository.findByTeacher(username,page);
	}

	@Override
	public Long countByStudentAndCourse(String cardNum, Long id) {
		return examPartRepository.countByExam_enrollment_student_cardNumberAndExam_enrollment_courseInstance_id(cardNum, id);
	}

	@Override
	public Long countByCourseInstance(Long id) {
		return  examPartRepository.countByCourseInstance(id);
	}

	@Override
	public Long countByTeacher(String username) {
		return examPartRepository.countByTeacher(username);
	}

	@Override
	public void deleteByCode(String code) {
		examPartRepository.deleteByCode(code);
	}

	@Override
	public Page<ExamPart> findByCodeAndStatus(String code, String status,Pageable page) {
		return examPartRepository.findByCodeAndExamPartStatus_code(code, status,page);
	}

	@Override
	public Long countByExamPart(String code, String status) {
		return examPartRepository.countByCodeAndExamPartStatus_code(code, status);
	}

}

