package com.example.EObrazovanje2022.dto;

public class UrlDTO {
	private String url;
	
	public UrlDTO(String url) {
		super();
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
