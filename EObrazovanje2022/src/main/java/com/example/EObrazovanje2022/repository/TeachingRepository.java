package com.example.EObrazovanje2022.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.EObrazovanje2022.model.Teaching;

public interface TeachingRepository extends JpaRepository<Teaching, Long> {
	List<Teaching> findByTeacher_user_username(String username);
	Teaching findByCourseInstance_id(Long courseId);
}
