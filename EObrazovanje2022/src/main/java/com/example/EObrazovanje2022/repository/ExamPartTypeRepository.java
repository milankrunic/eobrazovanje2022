package com.example.EObrazovanje2022.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje2022.model.ExamPartType;

public interface ExamPartTypeRepository extends JpaRepository<ExamPartType, Long> {
	ExamPartType findOneByCode(String code);
}

