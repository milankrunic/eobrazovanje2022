package com.example.EObrazovanje2022.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.EObrazovanje2022.model.TeachingType;

public interface TeachingTypeRepository extends JpaRepository<TeachingType, Long> {
	TeachingType findOneByCode(String code);
}
