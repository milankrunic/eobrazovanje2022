package com.example.EObrazovanje2022.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje2022.model.TypeDocument;

public interface TypeDocumentRepository extends JpaRepository<TypeDocument, Long> {
	TypeDocument findOneByCode(String id);
}
