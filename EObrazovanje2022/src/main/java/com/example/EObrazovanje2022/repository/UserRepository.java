package com.example.EObrazovanje2022.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje2022.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findOneByUsername(String username);
}
