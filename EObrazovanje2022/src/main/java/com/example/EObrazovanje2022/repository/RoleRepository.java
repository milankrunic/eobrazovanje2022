package com.example.EObrazovanje2022.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.example.EObrazovanje2022.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findOneByCode(String code);
}
