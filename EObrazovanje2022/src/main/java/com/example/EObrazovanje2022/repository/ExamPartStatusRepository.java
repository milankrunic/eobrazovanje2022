package com.example.EObrazovanje2022.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EObrazovanje2022.model.ExamPartStatus;

public interface ExamPartStatusRepository extends JpaRepository<ExamPartStatus, Long> {
	ExamPartStatus findOneByCode(String code);
}
