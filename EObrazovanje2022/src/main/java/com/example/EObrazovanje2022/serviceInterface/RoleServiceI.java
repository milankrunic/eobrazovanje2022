package com.example.EObrazovanje2022.serviceInterface;

import java.util.List;

import com.example.EObrazovanje2022.model.Role;

public interface RoleServiceI {
	
	public List<Role> findAll();
	
	public Role findById(Long id);
	
	public Role save(Role userRole);
	
	public void delete(Long id);
	
	public Role findByCode(String code);
}
