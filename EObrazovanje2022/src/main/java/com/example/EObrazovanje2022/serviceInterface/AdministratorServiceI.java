package com.example.EObrazovanje2022.serviceInterface;

import java.util.List;

import com.example.EObrazovanje2022.model.Administrator;

public interface AdministratorServiceI {

	public List<Administrator> findAll();
	
	public Administrator findById(Long id);
	
	public Administrator save(Administrator administrator);
	
	public void delete(Long id);
	
	public Administrator findByUser(String username);
}
