package com.example.EObrazovanje2022.serviceInterface; 

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.EObrazovanje2022.model.CourseSpecification;

public interface CourseSpecificationServiceInterface {

	public Page<CourseSpecification> findAll(Pageable page);
	
	public Page<CourseSpecification> findAll(String searchString,Pageable page);
	
	public CourseSpecification findById(Long id);
	
	public CourseSpecification save(CourseSpecification courseSpecification);
	
	public void delete(Long id);
	
	public Long count();

}

