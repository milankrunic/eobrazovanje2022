package com.example.EObrazovanje2022.serviceInterface; 

import java.util.List;

import com.example.EObrazovanje2022.model.Enrollment;

public interface EnrollmentServiceInterface {

	public List<Enrollment> findAll();
	
	public Enrollment findById(Long id);
	
	public Enrollment save(Enrollment e);
	
	public void delete(Long id);
	
	public Enrollment findByCourseInstanceAndStudent(Long idCourseInstance,String cardNumber);
}

